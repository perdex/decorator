"""Contains decorator functions"""

from time import perf_counter
import traceback
import logging
import sys
import tracemalloc

try:
    whatever()
except Exception as e:
    logging.error(traceback.format_exc())
    # Logs the error appropriately. 

def error_handler(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            print(traceback.format_exc())
            print(f"Error when running function: {func.__name__}")
            logging.error(func.__name__)
            logging.error(traceback.format_exc())
    return wrapper

def timed(func):
    def wrapper(*args, **kwargs):
        start_t = perf_counter()
        func(*args, **kwargs)
        end_t = perf_counter()
        t_diff = end_t - start_t
        print(f"Time took {t_diff:0.8f} for function: {func.__name__}")
    return wrapper

def mem_space(func):
    def wrapper(*args, **kwargs):
        #do something here
        func(*args, **kwargs)
        #do something here
        
    return wrapper

@mem_space
@error_handler
def divide(a, b):
    return a / b

@mem_space
@timed
def power(a, power):
    return a ** power

def main():
    
    print(divide(2, "b"))
    print(divide(2, 4))
    print(power(2, 7))    
    
    x = 5
    d = 3
    b = []
    print("Variable used",sys.getsizeof(b),"bytes of memory.")
    
    

if __name__ == "__main__":
    main()
